[![License Apache2](https://img.shields.io/badge/License-Apache2-blue.svg)](https://img.shields.io/badge/License-Apache2-blue.svg)

# About

Conventional Commits are cool but hard to remember. The `ccfrosting` app supports you in writing Conventional Commits compliant commit messages. Why not just using [commitizen](https://github.com/commitizen-tools/commitizen)? There are some short commings in the available tools which this tool tries to mitigate. Of course because Go is awesome.

