/*
Copyright © 2022 Christian Ege <ch@ege.io>

*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"github.com/AlecAivazis/survey/v2"
	"github.com/fatih/color"
)

var (
	SignTheCommit = false
	DryRun        = false
)

// commitCmd represents the commit command
var commitCmd = &cobra.Command{
	Use:   "commit",
	Short: "Create a new conventional commit",
	Long: `This is a helper which navigates you through the process of
	creating a commit message which applies to the conventional commits standard`,
	Run: func(cmd *cobra.Command, args []string) {
		highlight := color.New(color.Bold, color.FgMagenta).SprintFunc()
		qs := []*survey.Question{
			{
				Name: "type",
				Prompt: &survey.Select{
					Message: "Please select the commit type:",
					Help:    "The type of the commit please select one from the list",
					Options: []string{
						fmt.Sprintf("%s, A bug fix. Correlates with PATCH in SemVer", highlight("fix")),
						fmt.Sprintf("%s, A new feature. Correlates with MINOR in SemVer", highlight("feat")),
						fmt.Sprintf("%s, Documentation only changes", highlight("docs")),
						fmt.Sprintf("%s, Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)", highlight("style")),
						fmt.Sprintf("%s, A code change that neither fixes a bug nor adds a feature", highlight("refactor")),
						fmt.Sprintf("%s, Changes to our CI configuration files and scripts (example scopes: GitLabCI)", highlight("ci")),
					},
				},
			},
			{
				Name:   "scope",
				Prompt: &survey.Input{Message: "What is the scope of this change? (class or file name): (press [enter] to skip)"},
			},
			{
				Name: "title",
				Prompt: &survey.Input{
					Message:      "Write a short and imperative summary of the code changes: (lower case and no period)",
					PrintNewline: true,
				},
			},
			{
				Name: "body",
				Prompt: &survey.Multiline{
					Message:      "Provide additional contextual information about the code changes: ",
					PrintNewline: true,
				},
			},
			{
				Name: "breaking",
				Prompt: &survey.Confirm{
					Message: "Is this a BREAKING CHANGE? Correlates with MAJOR in SemVer: ",
				},
			},
		}

		answers := struct {
			Type           int    `survey:"type"`
			Scope          string `survey:"scope"`
			Title          string `survey:"title"`
			Body           string `survey:"body"`
			Breaking       bool   `survey:"breaking"`
			BreakingAnswer string
			Footer         string
		}{}

		// perform the questions
		err := survey.Ask(qs, &answers, survey.WithShowCursor(true))
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		if answers.Breaking {
			prompt := &survey.Input{
				Message: "Please describe the breaking change: ",
			}
			survey.AskOne(prompt, &answers.BreakingAnswer, survey.WithShowCursor(true))
		}

		prompt := &survey.Multiline{
			Message: "Footer. Information about reference issues that this commit closes:",
		}
		survey.AskOne(prompt, &answers.Footer, survey.WithShowCursor(true))

		fmt.Printf("What type: %d scope: %s \ntitle: %s \n\n", answers.Type, answers.Scope, answers.Title)
		fmt.Printf("\n%s\n", answers.Body)
		if answers.Breaking {
			fmt.Printf("\n%s\n", answers.BreakingAnswer)
		}
		if answers.Footer != "" {
			fmt.Printf("\n%s\n", answers.Footer)
		}
	},
}

func init() {
	rootCmd.AddCommand(commitCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// commitCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	commitCmd.Flags().BoolVarP(&SignTheCommit, "signoff", "s", false, "Sign the created commit message")
	commitCmd.Flags().BoolVarP(&DryRun, "dry-run", "", false, "Perform a dry run")
}
