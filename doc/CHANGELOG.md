## v0.1.0 (2022-04-11)

### Features

- Add a baseline for a Conventional Commits helper app in Go

## v0.0.1 (2022-04-11)
### Added
- A baseline for a Conventional Commits helper application in Go